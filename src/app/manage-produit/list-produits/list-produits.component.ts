import {Component, OnInit} from '@angular/core';
import { Produit } from '../../manage-produit/models/Produit';
import Swal from 'sweetalert2';
import { Form } from '@angular/forms';
import { ProduitService } from '../../manage-produit/services/produit.service';
import { ConditionalExpr } from '@angular/compiler';
@Component({
  selector: 'app-list-produits',
  templateUrl: './list-produits.component.html',
  styleUrls: ['./list-produits.component.css']
})
export class ListProduitsComponent implements OnInit {
  produit : Produit = new Produit();
  produits:Produit[];
  pro_len:number;
  constructor(private service:ProduitService) {
  }

  ngOnInit() {
  this.service.getProduits().subscribe
  (prod=>{this.produits=prod;
  this.pro_len=this.produits.length;});
  }
  onDelete(produit:Produit){
    if (confirm('Are you sure you want to delete this element ?'))
    {
    this.service.deleteProduct(produit.id).subscribe(()=>{
      this.produits.forEach((current,index)=>
      {
        if(produit.id == current.id){
          // console.log("id",produit.id);
          // console.log("index",index);
          this.produits.splice(index,1);
          Swal.fire(
            'Success!',
            'Product deleted!',
            'success'
          )
        }
        else {
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'An error has occured!'
          });}
      });
    });
    }
    Swal.fire()
  }

}
