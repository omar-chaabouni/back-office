import { Component, OnInit } from '@angular/core';
import {Produit} from '../models/Produit';
import { Form } from '@angular/forms';
import { ProduitService } from '../services/produit.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-ajout-produit',
  templateUrl: './ajout-produit.component.html',
  styleUrls: ['./ajout-produit.component.css']
})
export class AjoutProduitComponent implements OnInit {
  produit : Produit = new Produit();
  // produits:Produit[];
  constructor(private service:ProduitService) {}
  ngOnInit() { 
  }
  onSubmit(){
  console.log(this.produit);
  this.service.saveProduit(this.produit as Produit).subscribe
  (prod=>{console.log(prod)});
  Swal.fire('Success!',
  'Your product has been added to your database! Well done omar!',
  'success',
  );
  }
}
