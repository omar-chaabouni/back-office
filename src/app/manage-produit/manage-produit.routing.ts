// Layouts
import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import { AjoutProduitComponent } from "../manage-produit/ajout-produit/ajout-produit.component";
import {ListProduitsComponent} from "../manage-produit/list-produits/list-produits.component";
// import { ManageProduitModule } from "../manage-produit/manage-produit.module";
export const routes: Routes = [

{
    path: 'ajout-produit',
    component: AjoutProduitComponent
}, {
    path: 'list-produits',
    component: ListProduitsComponent
}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ManageProduitRouting {
}
