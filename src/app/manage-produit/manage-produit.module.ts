import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AjoutProduitComponent } from './ajout-produit/ajout-produit.component';
import { ListProduitsComponent } from './list-produits/list-produits.component';
import { ManageProduitRouting } from './manage-produit.routing';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [AjoutProduitComponent, ListProduitsComponent],
  imports: [
    ManageProduitRouting,
    CommonModule, SharedModule
  ]
})
export class ManageProduitModule { }
